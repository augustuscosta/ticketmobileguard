package br.com.otgmobile.ticketmobileguard.activity;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.adapter.ConteudoAdapter;
import br.com.otgmobile.ticketmobileguard.database.DatabaseAccessProvider;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.service.SyncService;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;
import br.com.otgmobile.ticketmobileguard.util.Session;

public class EventoListActivity extends ListActivity {

	private EditText filterText;
	private String textFilter;
	private List<Conteudo> conteudos;
	private DatabaseAccessProvider provider;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);
		SyncService.startService(this);
		filterText = (EditText) findViewById(R.id.filterText);
		filterText.addTextChangedListener(filterTextWatcher);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(filterText.getText().length() > 0){
			queryForFilter(filterText.getText().toString());
		}else{
			getEvents();
		}
	}
	
	@Override
	protected void onDestroy() {
		SyncService.stopService(this);
		provider().clearObjectCahe();
		Session.setToken("", this);
		super.onDestroy();
	}
	
	@Override
	protected void onPause() {
		provider = null;
		super.onPause();
	}
	
	@Override
	public void onBackPressed() {
		showDialogBeforeExiting();
	}

	private void showDialogBeforeExiting() {
		final AlertDialog.Builder d = new AlertDialog.Builder(this);
		d.setTitle(R.string.deseja_sair);
		d.setMessage(R.string.se_finalizar_a_sess_o_perder_todos_os_dados_n_o_enviados_deseja_finalizar);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				finish();
			}
		});
		d.setNeutralButton(R.string.nao, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		d.show();
	}
	
	private void getEvents() {
		conteudos = provider().getAllEventos();
		setListAdapter(new ConteudoAdapter(conteudos, this));
	}
	
	private void queryForFilter(String textFilter) {
		conteudos = provider().getAllConteudoByCriteria(textFilter);
		setListAdapter(new ConteudoAdapter(conteudos, this));
	}


	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(this);
		}

		return provider;
	}
	
	protected void onListItemClick(android.widget.ListView l, android.view.View v, int position, long id) {
		Conteudo conteudo = conteudos.get(position);
		Intent intent = new Intent(this,EventoPreviewActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	TextWatcher filterTextWatcher = new TextWatcher() {

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			textFilter = s.toString();
			if(s == null || s.length() == 0){
				getEvents();
			}else{
				queryForFilter(textFilter);				
			}
		}

		

		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// DO NOTHING
		}

		public void afterTextChanged(Editable s) {
			// DO NOTHING
		}
	};

}
