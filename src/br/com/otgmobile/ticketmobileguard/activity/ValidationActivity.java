/**
 * 
 */
package br.com.otgmobile.ticketmobileguard.activity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.database.DatabaseAccessProvider;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

/**
 * @author brunoramosdias
 *
 */
public class ValidationActivity extends Activity {
	
	private Spinner ingressoCountSpinner;
	private Ingresso ingresso;
	private int available;
	String[] populateSpinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ingresso_validator);
		ingressoCountSpinner = (Spinner) findViewById(R.id.ingressoCountToAuthenticate);
		handleIntent();
	}

	private void handleIntent() {
		ingresso = (Ingresso) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		available = getIntent().getIntExtra(ConstUtil.AVAILABLE_COUNT, 0);
		populateSpinner();
	}

	private void populateSpinner() {
		populateSpinner = new String[available+1];
		
		for (int i = 0; i < populateSpinner.length; i++) {
			populateSpinner[i] = Integer.toString(i);
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_target,populateSpinner);
		adapter.setDropDownViewResource(R.layout.spinner_target);
		ingressoCountSpinner.setAdapter(adapter);
	}
	
	public void onClickValidate(View view){
		showDialogToConfirm();
	}

	private void showDialogToConfirm() {
		final Dialog d = new Dialog(this);
		d.setContentView(R.layout.confirm_ingresso_dialog);
		d.setTitle(R.string.confirmar);
		final TextView textView = (TextView) d.findViewById(R.id.dialog_Text);
		textView.setText(getString(R.string.validar_) +ingressoCountSpinner.getSelectedItemPosition()+getString(R.string._acessos ));
		final Button confirm = 	(Button) d.findViewById(R.id.confirm);
		final Button cancel  = (Button) d.findViewById(R.id.cancel);
		confirm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				createAcessos();
				d.dismiss();
			}
		});
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				d.dismiss();
			}
		});
		
		d.show();
		
	}

	protected void createAcessos() {
		if(ingresso.getAcessos() == null){
			Collection<Acesso> acessos = new ArrayList<Acesso>();
			int count = Integer.parseInt(ingressoCountSpinner.getSelectedItem().toString());
			for (int i = 0; i < count; i++) {
				Acesso acesso = new Acesso();
				acesso.setIngresso(ingresso);
				acessos.add(acesso);
			}
			
			ingresso.setAcessos(acessos);
		}else{
			Collection<Acesso> acessos = ingresso.getAcessos();
			int count = Integer.parseInt(ingressoCountSpinner.getSelectedItem().toString());
			for (int i = 0; i < count; i++) {
				Acesso acesso = new Acesso();
				acesso.setIngresso(ingresso);
				acesso.setSent(0);
				acesso.setData(new Date());
				acessos.add(acesso);
			}
			ingresso.setAcessos(acessos);
		}
		try {
			persistIngressoAndFinish();
		} catch (SQLException e) {
			LogUtil.e("erro ao persistir os acessos", e);
		}
	}

	private void persistIngressoAndFinish() throws SQLException {
		DatabaseAccessProvider provider = new DatabaseAccessProvider(this);
		for(Acesso acesso : ingresso.getAcessos()){
			provider.acessoDao().create(acesso);
		}
		provider.ingressoDao().createOrUpdate(ingresso);
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, ingresso);
		intent.putExtras(bundle);
		setResult(RESULT_OK, intent);
		finish();
	}
	

}
