package br.com.otgmobile.ticketmobileguard.activity;

import java.sql.SQLException;
import java.util.List;

import roboguice.inject.InjectView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.cloud.IngressoCloud;
import br.com.otgmobile.ticketmobileguard.cloud.RestClient;
import br.com.otgmobile.ticketmobileguard.database.DatabaseAccessProvider;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.util.AppHelper;
import br.com.otgmobile.ticketmobileguard.util.AutenticationUtil;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

/**
 * @author brunoramosdias
 *
 */

public class EventoPreviewActivity extends Activity {

	@InjectView(R.id.tituloText) TextView tituloText;
	@InjectView(R.id.enderecoText) TextView enderecoText;
	@InjectView(R.id.ingressoText) TextView ingressoText;
	private DatabaseAccessProvider provider;
	private Conteudo conteudo;
	private GetIngressosTask getIngressosTask;
	private List<Ingresso> ingressos;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.preview_reader);
		setComponents();
		handleIntent();
	}

	private void setComponents() {
		tituloText = (TextView)  findViewById(R.id.tituloText);
		enderecoText = (TextView) findViewById(R.id.enderecoText);
		ingressoText = (TextView) findViewById(R.id.ingressoText);
		
	}

	@Override
	protected void onPause() {
		stopGetIngressosTask();
		super.onPause();
	}

	private void handleIntent() {
		conteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);

		if(loadIngressos() == null || loadIngressos().isEmpty()){
			executeGetIngressosTask();

		}else{
			fillFields();			
		}
	}

	private void executeGetIngressosTask() {
		if(getIngressosTask == null){
			getIngressosTask = new GetIngressosTask(this);
		}

		getIngressosTask.execute();
	}

	private void stopGetIngressosTask() {
		if(getIngressosTask != null){
			getIngressosTask.cancel(true);
		}

		getIngressosTask = null;
	}


	class GetIngressosTask extends AsyncTask<String, Void, RestClient> {
		ProgressDialog mDialog;
		Context context;
		String erroMessage = null;

		IngressoCloud cloud;



		GetIngressosTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.processing)+" "+getString(R.string.lista_de_eventos), false, true);
			mDialog.setCancelable(true);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		@Override
		public RestClient doInBackground(String... params){
			return getIngressos();
		}

		RestClient getIngressos(){
			cloud = new IngressoCloud(context);
			try {
				cloud.getIngressos(conteudo.getId());
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				LogUtil.e("erro ao pegar ingressos", e);
				return cloud;
			}
			if(cloud.getResponseCode() != 200){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

		@Override
		public void onPostExecute(RestClient cloud) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			getIngressosTask = null;
			if (erroMessage != null) {
				AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),getResources().getString(R.string.error_conection));
			}else{
				fillFields();
			}
			mDialog.dismiss();
		}

	}

	private void fillFields() {
		ingressos = loadIngressos();
		conteudo.setIngressos(ingressos);

		if(ingressos != null){
			ingressoText.setText(Integer.toString(ingressos.size()));	
		}else{
			ingressoText.setText("0");
		}

		if(conteudo.getTitulo()!= null){
			tituloText.setText(conteudo.getTitulo());
		}

		if(conteudo.getEndereco()!= null){
			enderecoText.setText(conteudo.getEndereco());
		}

	}

	private List<Ingresso> loadIngressos() {
		return provider().getIngressoFromEvent(conteudo.getId());
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(this);
		}

		return provider;
	}

	public void onClickRead(View view){
		openQrCodeScan();
	}

	private void openQrCodeScan() {
		final Intent intent = new Intent(this, com.google.zxing.client.android.CaptureActivity.class);
		intent.putExtra("SCAN_MODE", "QR_CODE_MODE|ONE_D_MODE");
		startActivityForResult(intent, ConstUtil.QR_CODE_SCAN);
	}

	public void onClickViewIngressos(View view){
		viewIngressos();
	}

	private void viewIngressos() {
		Intent intent = new Intent(this, IngressoListActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, conteudo);
		intent.putExtras(bundle);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ConstUtil.QR_CODE_SCAN:

			if(data != null && data.hasExtra(ConstUtil.SCAN_RESULT)){
				final String contents = data.getStringExtra(ConstUtil.SCAN_RESULT);
					proccesIngressoFromQrCode(contents);									
			}else{
				Toast.makeText(this, R.string.n_o_foi_possivel_ler_o_qrcode_ou_a_opera_o_foi_cancelada, Toast.LENGTH_SHORT).show();
			}
			break;

		case ConstUtil.INGRESSO_PREVIEWER:
			if(resultCode == RESULT_OK){
				Ingresso ingresso2 = (Ingresso) data.getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
				try {
					provider().ingressoDao().update(ingresso2);
					fillFields();
				} catch (SQLException e) {
					LogUtil.e("erro ao atualizarr os ingressos", e);
				}
			}
			break;
		case 0x0ba7c0de:
			if(data != null && data.hasExtra(ConstUtil.SCAN_RESULT)){
				final String contents = data.getStringExtra(ConstUtil.SCAN_RESULT);
					proccesIngressoFromQrCode(contents);									
			}else{
				Toast.makeText(this, R.string.n_o_foi_possivel_ler_o_qrcode_ou_a_opera_o_foi_cancelada, Toast.LENGTH_SHORT).show();
			}
			
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private void proccesIngressoFromQrCode(final String contents) {
		Ingresso ingresso = provider().getIngresso(contents);
		if(ingresso == null){
			Toast.makeText(this, R.string.ingresso_n_o_encontrado, Toast.LENGTH_LONG).show();
			return;
		}
		verifyIngresso(ingresso);				
	}

	private void verifyIngresso(Ingresso ingresso) {
		Ingresso fromDb = provider().getIngresso(ingresso.getId());
		if(fromDb.equals(ingresso)){
			if(AutenticationUtil.getAcessos(fromDb) > 0 && ingresso.getConteudoId().equals(conteudo.getId())){
				previewIngresso(fromDb);
			}else{
				AppHelper.getInstance().presentError(this, getString(R.string.atencao), getString(R.string.ingresso_n_o_v_lido_para_esta_data));
			}
		}else{
			AppHelper.getInstance().presentError(this, getString(R.string.atencao), getString(R.string.ingresso_invalido));
		}
	}

	private void previewIngresso(Ingresso obj) {
		Intent intent = new Intent(this,IngressoPreviewer.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, obj);
		bundle.putSerializable(ConstUtil.CONTEUDO, conteudo);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.INGRESSO_PREVIEWER);
	}

}
