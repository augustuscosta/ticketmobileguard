package br.com.otgmobile.ticketmobileguard.activity;

import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.adapter.IngressoAdapter;
import br.com.otgmobile.ticketmobileguard.database.DatabaseAccessProvider;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;

public class IngressoListActivity extends ListActivity {

	private List<Ingresso> ingressos;
	private Conteudo conteudo;
	private String textFilter;
	private DatabaseAccessProvider provider;

	private TextView filterText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ingresso_list);
		handleIntent();
		setFilter();
	}

	private void setFilter() {
		filterText = (EditText) findViewById(R.id.filterText);
		filterText.addTextChangedListener(filterTextWatcher);
	}

	private void handleIntent() {
		conteudo = (Conteudo) getIntent().getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		checkFillAndSetAdapter();
	}

	private void checkFillAndSetAdapter() {
		getIngressos();
		if(ingressos == null || ingressos.isEmpty()){
			Toast.makeText(this, R.string.sem_ingressos_comprados_neste_evento, Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		setAdapter();
	}

	private void setAdapter() {
		setListAdapter(new IngressoAdapter(ingressos, this));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent intent = new Intent(this,IngressoPreviewer.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, ingressos.get(position));
		bundle.putSerializable(ConstUtil.CONTEUDO, conteudo);
		intent.putExtras(bundle);
		startActivity(intent);
		super.onListItemClick(l, v, position, id);
	}


	TextWatcher filterTextWatcher = new TextWatcher() {

		public void onTextChanged(CharSequence s, int start, int before, int count) {
			textFilter = s.toString();
			if(s == null || s.length() == 0){
				getIngressos();
			}else{
				queryForFilter(textFilter);				
			}
		}


		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// DO NOTHING
		}

		public void afterTextChanged(Editable s) {
			// DO NOTHING
		}
	};

	private void queryForFilter(String textFilter) {
		ingressos = provider().getAllIngressoByCriteriaAndConteudo(textFilter, conteudo.getId());
		setAdapter();
	}

	private void getIngressos() {
		ingressos = provider().getIngressoFromEvent(conteudo.getId());
		setAdapter();
	}

	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new DatabaseAccessProvider(this);
		}

		return provider;
	}

}
