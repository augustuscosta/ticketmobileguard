package br.com.otgmobile.ticketmobileguard.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.cloud.ConteudoCloud;
import br.com.otgmobile.ticketmobileguard.cloud.RestClient;
import br.com.otgmobile.ticketmobileguard.cloud.SessionCloud;
import br.com.otgmobile.ticketmobileguard.util.AppHelper;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;
import br.com.otgmobile.ticketmobileguard.util.Session;

public class MainActivity extends Activity {
	
	private TextView email;
	private TextView password;
	private static LoginTask loginTask;
	private static SyncTask syncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LogUtil.initLog(this, "TICKET_MOBILE_GUARD");
        setContentView(R.layout.activity_main);
        email = (TextView) findViewById(R.id.email);
        password = (TextView) findViewById(R.id.password);
        checkToken();
    }
    
    private void checkToken() {
		if(Session.getToken(this) != null){
			startSystem();
		}
	}

	@Override
    protected void onPause() {
    	stopLoginTask();
    	stopSyncTask();
    	super.onPause();
    }
    
    public void login(View view){
    	if(email.getText().length() > 0 && password.getText().length() > 0){
    		executeLoginTask();
    	}else{
    		AppHelper.getInstance().presentError(this,getResources().getString(R.string.error_missing_info),getResources().getString(R.string.error_missing_info_login));
    	}
    	
    	
    }
    
    private void executeLoginTask() {
    	stopLoginTask();
    	if(loginTask == null){
    		loginTask = new LoginTask(this);
    	}
    	
    	loginTask.execute();

	}
    
    
    private void stopLoginTask() {
    	if(loginTask != null){
    		loginTask.cancel(true);
    	}
    	
    	loginTask = null;
    	
    }
    
    private void executeSyncTask() {
    	if(syncTask == null){
    		syncTask = new SyncTask(this);
    	}
    	
    	syncTask.execute();
    	
    }
    
    
    private void stopSyncTask() {
    	if(syncTask != null){
    		syncTask.cancel(true);
    	}
    	
    	syncTask = null;
    	
    }

    
    private void startSystem() {
		Intent intent = new Intent(MainActivity.this,EventoListActivity.class);
		startActivity(intent);
	}


	 class LoginTask extends AsyncTask<String, Void, RestClient> {
		ProgressDialog mDialog;
		Context context;
		String erroMessage = null;

		SessionCloud cloud;
		

		LoginTask(Context context) {
			this.context = context;
			mDialog = ProgressDialog.show(context, "", getString(R.string.logging), false, true);
			mDialog.setCancelable(true);
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		}

		@Override
		public RestClient doInBackground(String... params){
			return login();
		}

		RestClient login(){
			cloud = new SessionCloud(context);
			try {
				cloud.login(email.getText().toString(), password.getText().toString());
			} catch (Throwable e) {
				erroMessage = e.getLocalizedMessage();
				return cloud;
			}
			if(cloud.getResponseCode() != 200){
				erroMessage = cloud.getErrorMessage();
				return cloud;
			}
			return cloud;
		}

		@Override
		public void onPostExecute(RestClient cloud) {
			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
			loginTask = null;
			mDialog.dismiss();
			if (erroMessage != null) {
				if(cloud.getResponseCode() == ConstUtil.INVALID_TOKEN){
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),getResources().getString(R.string.wrong_user_or_password));
				}else{
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),erroMessage);
				}
			} else {
				executeSyncTask();
			}
		}

	}
    
     class SyncTask extends AsyncTask<String, Void, RestClient> {
 		ProgressDialog mDialog;
 		Context context;
 		String erroMessage = null;

 		ConteudoCloud cloud;
 		

 		SyncTask(Context context) {
 			this.context = context;
 			mDialog = ProgressDialog.show(context, "", getString(R.string.processing)+" "+getString(R.string.lista_de_eventos), false, true);
 			mDialog.setCancelable(true);
 			getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
 		}

 		@Override
 		public RestClient doInBackground(String... params){
 			return sincronize();
 		}

 		RestClient sincronize(){
 			cloud = new ConteudoCloud(context);
 			try {
 				cloud.getConteudos();
 			} catch (Throwable e) {
 				erroMessage = e.getLocalizedMessage();
 				return cloud;
 			}
 			if(cloud.getResponseCode() != 200){
 				erroMessage = cloud.getErrorMessage();
 				return cloud;
 			}
 			return cloud;
 		}

 		@Override
 		public void onPostExecute(RestClient cloud) {
 			getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
 			syncTask = null;
 			if (erroMessage != null) {
					AppHelper.getInstance().presentError(context,getResources().getString(R.string.error_conection),getResources().getString(R.string.error_conection));
			}else{
				startSystem();
			}
 			mDialog.dismiss();
 		}

 	}
    
}
