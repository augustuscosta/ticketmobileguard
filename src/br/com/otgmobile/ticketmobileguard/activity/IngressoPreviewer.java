package br.com.otgmobile.ticketmobileguard.activity;

import java.util.ArrayList;
import java.util.Collection;

import roboguice.activity.RoboActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Entrada;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.model.TipoDeIngresso;
import br.com.otgmobile.ticketmobileguard.model.Usuario;
import br.com.otgmobile.ticketmobileguard.util.AppHelper;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;

public class IngressoPreviewer extends RoboActivity {
	
	private TextView clienteText;
	private TextView cpfText;
	private TextView rgText;
	private TextView enderecoText;
	private TextView entradaText;
	private TextView tipoText;
	private TextView dataText;
	
	private Ingresso ingresso;
	private Conteudo conteudo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ingresso_previewer);
		setComponentsOnView();
		handleIntent(getIntent());
	}

	private void setComponentsOnView() {
		clienteText = (TextView) findViewById(R.id.clienteText);
		cpfText = (TextView) findViewById(R.id.cpf);
		rgText = (TextView) findViewById(R.id.rg);
		enderecoText= (TextView) findViewById(R.id.enderecoText);
		entradaText = (TextView) findViewById(R.id.entradaText);
		tipoText = (TextView) findViewById(R.id.tipoText);
		dataText = (TextView) findViewById(R.id.dataText);
	}

	private void handleIntent(Intent intent) {
		ingresso = (Ingresso) intent.getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
		conteudo = (Conteudo) intent.getSerializableExtra(ConstUtil.CONTEUDO);
		setIngressoOnComponents();
	}
	
	@SuppressWarnings("deprecation")
	private void setIngressoOnComponents(){
		if(ingresso == null) return;
		
		Usuario usuario = ingresso.getUsuario();
		TipoDeIngresso tipo = ingresso.getTipoDeIngresso();
		
		if(usuario != null){
			if(usuario.getName() != null){
				clienteText.setText(usuario.getName());
			}
			
			if(usuario.getCpf() != null){
				cpfText.setText(usuario.getCpf());
			}else{
				cpfText.setText(R.string.sem_cpf);				
			}
			
			if(usuario.getRg() != null){
				rgText.setText(usuario.getRg());
			}else{
				rgText.setText(R.string.sem_rg);
			}
		}
		
		if(tipo != null){
			if(tipo.getTitulo() != null){
				tipoText.setText(tipo.getTitulo());
			}
			Collection<Entrada> entradas = tipo.getEntradas();
			
			if(entradas != null && ingresso.getAcessos()!= null){
				int disponiveis = entradas.size() - ingresso.getAcessos().size();
				entradaText.setText(Integer.toString(disponiveis));
			}else if(entradas != null){
				entradaText.setText(Integer.toString(entradas.size()));
			}else{
				entradaText.setText("0");							
			}
		}
		
		if(conteudo != null){
			if(conteudo.getEndereco()!= null) enderecoText.setText(conteudo.getEndereco());
			if(conteudo.getData()!= null) dataText.setText(conteudo.getData().toLocaleString());
		}
	}
	
	public void onClickValidate(View view){
		validateEntradas();
	}

	private void validateEntradas() {
		int disponiveis = 0;
		Collection<Entrada> entradasTotais =  ingresso.getTipoDeIngresso().getEntradas();
		Collection<Entrada> entradasDoDia = getEntradasDodia(entradasTotais);
		if(!entradasDoDia.isEmpty()){
			if(ingresso.getAcessos() == null || ingresso.getAcessos().isEmpty()){
				disponiveis = entradasDoDia.size(); 				
			}else{
				disponiveis = entradasDoDia.size() - ingresso.getAcessos().size(); 								
			}
		}
		
		if(disponiveis < 1){
			AppHelper.getInstance().presentError(this, getString(R.string.acesso_negado), getString(R.string.nenhum_acesso_dispon_vel));			
			return;
		}
		
		Intent intent = new Intent(this,ValidationActivity.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable(ConstUtil.SERIALIZABLE_NAME, ingresso);
		bundle.putInt(ConstUtil.AVAILABLE_COUNT, disponiveis);
		intent.putExtras(bundle);
		startActivityForResult(intent, ConstUtil.VALIDATION);
	}

	private Collection<Entrada> getEntradasDodia(Collection<Entrada> entradasTotais) {
		Collection<Entrada> doDia = new ArrayList<Entrada>();
		for(Entrada entrada :entradasTotais){
			if(entrada.getData().getTime()+ConstUtil.A_DAY_IN_MILLS > AppHelper.getCurrentTime() && AppHelper.getCurrentTime()  > entrada.getData().getTime()){
				doDia.add(entrada);
			}
			
		}
		return doDia;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case ConstUtil.VALIDATION:
			switch (resultCode) {
			case RESULT_OK:
				ingresso = (Ingresso) data.getSerializableExtra(ConstUtil.SERIALIZABLE_NAME);
				setIngressoOnComponents();
				break;

			default:
				break;
			}
			
			
			break;

		default:
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

}
