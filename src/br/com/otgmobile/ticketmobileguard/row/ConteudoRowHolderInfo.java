package br.com.otgmobile.ticketmobileguard.row;

import java.util.List;

import android.graphics.Color;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;

public class ConteudoRowHolderInfo {
	
	
	public TextView titulo;
	public TextView description;
	
	public void drawRow(Conteudo obj){
		if(obj!= null){
			if(obj.getTitulo() != null){
				titulo.setText(obj.getTitulo());
			}else{
				titulo.setText("");				
			}
			
			if(obj.getEndereco() != null){
				description.setText(obj.getEndereco());
			}else{
				description.setText("");				
			}
			
			if(obj.getIngressos() == null || obj.getIngressos().isEmpty()){
				titulo.setTextColor(Color.WHITE);
				description.setTextColor(Color.WHITE);
			}else{
				checkUnsentIngressos(obj.getIngressos());
			}
			
		}
	}

	private void checkUnsentIngressos(List<Ingresso> ingressos) {
		for(Ingresso ingresso: ingressos){
			if(ingresso.getAcessos() != null && !ingresso.getAcessos().isEmpty()){
				for(Acesso acesso:ingresso.getAcessos()){
					if(acesso.getSent()!= null &&acesso.getSent().equals(0)){
						titulo.setTextColor(Color.RED);
						description.setTextColor(Color.RED);
						return;
					}
				}
			}
		}
		
		titulo.setTextColor(Color.WHITE);
		description.setTextColor(Color.WHITE);
		
	}

}
