package br.com.otgmobile.ticketmobileguard.row;

import java.util.Collection;

import android.graphics.Color;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;

/**
 * @author brunoramosdias
 *
 */

public class IngressoRowHolderInfo {


	public TextView tipo;
	public TextView valor;
	public TextView comprador;
	public TextView tipoLabel;
	public TextView valorLabel;
	public TextView compradorLabel;
	private int textColor = Color.WHITE;

	public void drawRow(Ingresso obj){
		if(obj != null){
			setTextColor(obj);
			setLabelsColor();
			setTipodeIngressoNome(obj);
			setIngressoPreco(obj);
			setCompradorName(obj);
		}
	}
	
	private void setTextColor(Ingresso obj) {
		if(checkUnsent(obj.getAcessos())){
			textColor = Color.RED;
		}
	}
	
	private void setLabelsColor() {
		 tipoLabel.setTextColor(textColor);
		 valorLabel.setTextColor(textColor);
		 compradorLabel.setTextColor(textColor);
	}

	private void setTipodeIngressoNome(Ingresso obj) {
		if(obj.getTipoDeIngresso()!= null && obj.getTipoDeIngresso().getTitulo() != null){
			tipo.setText(obj.getTipoDeIngresso().getTitulo());
			tipo.setTextColor(textColor);
		}else{
			tipo.setTextColor(textColor);
			tipo.setText("");
		}
	}

	private void setIngressoPreco(Ingresso obj) {
		if(obj.getTipoDeIngresso()!= null && obj.getTipoDeIngresso().getValor() != null){
			valor.setText(obj.getTipoDeIngresso().getValor().toString());
			valor.setTextColor(textColor);
		}else{
			valor.setTextColor(textColor);
			valor.setText("");
		}
	}

	private void setCompradorName(Ingresso obj) {
		if(obj.getUsuario()!= null && obj.getUsuario().getName() != null){
			comprador.setText(obj.getUsuario().getName());
			comprador.setTextColor(textColor);
		}else{
			comprador.setTextColor(textColor);
			comprador.setText("");
		}
	}

	private boolean checkUnsent(Collection<Acesso> acessos) {
		if(acessos == null || acessos.isEmpty()){
			return false;
		}
		
		for(Acesso acesso : acessos){
			if(acesso.getSent()  == null || acesso.getSent() == 0){
				return true;
			}
		}
		return false;
	}

}
