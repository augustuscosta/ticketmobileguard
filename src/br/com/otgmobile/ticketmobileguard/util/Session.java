package br.com.otgmobile.ticketmobileguard.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

public class Session {
	

	public static final String PREFS 						 = "SESSION_PREFS";
	public static final String SERVER_PREFS_KEY 			 = "SERVER_PREFS_KEY";
	public static final String SOCKET_SERVER_PREFS_KEY 		 = "SOCKET_SERVER_PREFS_KEY";
	public static final String LAST_SYNC					 = "LAST_SYNC";
	public static final String TOKEN_PREFS_KEY 				 = "REST_TOKEN_PREFS_KEY";
	public static final String QRCODE 				 		 = "QRCODE";

	private static SharedPreferences settings;
 
	private static SharedPreferences getSharedPreferencesInstance(Context context){
		if(settings == null){
			settings = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
		}
		return settings;
	}
	
	public static String getDeviceId(Context context){
		TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		String szImei = TelephonyMgr.getDeviceId();
		return szImei;
	}
	
	public static void setServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getServer(Context context){				
		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://23.23.89.95:3001");
//		return getSharedPreferencesInstance(context).getString(SERVER_PREFS_KEY, "http://192.168.2.4:3000");
	}
	
	public static void setSocketServer(String server,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(SOCKET_SERVER_PREFS_KEY, server);
		editor.commit();
	}
	
	public static String getToken(Context context){
		String token = getSharedPreferencesInstance(context).getString(TOKEN_PREFS_KEY, null);
		if(token == null || token.length() < 1 ){
			return null;
		}
		return token;
	}
	
	public static void setToken(String token,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(TOKEN_PREFS_KEY, token);
		editor.commit();
	}
	public static String getLastQrCode(Context context){
		String token = getSharedPreferencesInstance(context).getString(QRCODE, null);
		if(token == null || token.length() < 1 ){
			return null;
		}
		return token;
	}
	
	public static void setQRCODE(String token,Context context){
		settings = getSharedPreferencesInstance(context);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(QRCODE, token);
		editor.commit();
	}
	
	public static void clearSession(Context context){
		Session.setToken("", context);
	}
	

}
