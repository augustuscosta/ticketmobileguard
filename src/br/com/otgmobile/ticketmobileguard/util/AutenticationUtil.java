package br.com.otgmobile.ticketmobileguard.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Entrada;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;

public class AutenticationUtil {

	
	public static Integer getAcessos(Ingresso ingresso){
		int disponiveis = 0;
		Collection<Entrada> entradasTotais =  ingresso.getTipoDeIngresso().getEntradas();
		Collection<Entrada> entradasDoDia = getEntradasDodia(entradasTotais);
		if(!entradasDoDia.isEmpty()){
			if(ingresso.getAcessos() == null || ingresso.getAcessos().isEmpty()){
				disponiveis = entradasDoDia.size(); 				
			}else{
				disponiveis = calculateAcessosDoDia(ingresso, entradasDoDia); 								
			}
		}
		
		return disponiveis;
	}


	private static int calculateAcessosDoDia(Ingresso ingresso,Collection<Entrada> entradasDoDia) {
		int disponiveis = 0;
		List<Acesso> doDia = new ArrayList<Acesso>();
		for(Acesso acesso: ingresso.getAcessos()){
			if(getValidityForAcesso(acesso)){
				doDia.add(acesso);
			}
		}
		
		disponiveis = entradasDoDia.size() - doDia.size();
		return disponiveis;
	}

	private static boolean getValidityForAcesso(Acesso acesso) {
		return acesso.getData().getTime()+ConstUtil.A_DAY_IN_MILLS > AppHelper.getCurrentTime() && AppHelper.getCurrentTime()  > acesso.getData().getTime();
	}
	
	private static Collection<Entrada> getEntradasDodia(Collection<Entrada> entradasTotais) {
		Collection<Entrada> doDia = new ArrayList<Entrada>();
		for(Entrada entrada :entradasTotais){
			if(entrada.getData().getTime()+ConstUtil.A_DAY_IN_MILLS > AppHelper.getCurrentTime() && AppHelper.getCurrentTime()  > entrada.getData().getTime()){
				doDia.add(entrada);
			}
			
		}
		return doDia;
	}

}
