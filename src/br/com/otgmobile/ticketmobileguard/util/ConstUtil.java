package br.com.otgmobile.ticketmobileguard.util;

import br.com.otgmobile.ticketmobileguard.R;

public class ConstUtil {

	public static final String COLON = ":";
	public static final String HIFEN = "-";
	public static final String SLASH = "/";
	public static final String TIME = "T";
	public static final String DOUBLESPACES ="  ";

	// Server Messages
	
	public static final int INVALID_TOKEN	 = 302;
	public static final String SERIALIZABLE_NAME = "serializable_object";
	public static final int INGRESSO_READER = R.layout.event_list;
	public static final int INGRESSO_PREVIEWER = R.layout.ingresso_previewer;
	public static final String CONTEUDO = "conteudo";
	public static final int QR_CODE_SCAN = 702;
	public static final Long A_DAY_IN_MILLS = (long)86400000;
	public static final int VALIDATION = 456789;
	public static final String AVAILABLE_COUNT = "AVAILABLE_COUNT";
	public static final String SCAN_RESULT = "SCAN_RESULT";
	
}
