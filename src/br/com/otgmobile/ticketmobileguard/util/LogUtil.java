package br.com.otgmobile.ticketmobileguard.util;

import java.util.Calendar;

import android.content.Context;
import android.util.Log;

public class LogUtil {


	private static String TAG = null;
	private static StringBuilder builder;

	/**
	 * Inicializa o log 
	 * 
	 * @param context
	 * @param tag - Tag na qual o log ira imprimir.
	 */
	public static void initLog(final Context context, final String tag) {
		String applicationLabel;
		if ( StringUtil.isValid(tag) ) {
			applicationLabel = tag;
		} else {
			applicationLabel = (String) context.getPackageManager().getApplicationLabel(context.getApplicationInfo());
		}
		
		TAG = applicationLabel;
		builder = new StringBuilder();
	}
	
	/**
	 * Muda a tag que esta sendo logada.
	 * 
	 * @param tag
	 */
	public static void changeTag(final String tag) {
		if ( StringUtil.isValid(tag) ) {
			TAG = tag;
		}
	}

	public static void d(String message) {
		d(message, null);
	}

	public static void d(String message, Throwable e) {
		Log.d(TAG, addTime(message), e);
	}

	public static void i(String message) {
		i(message, null);
	}

	public static void i(String message, Throwable e) {
		Log.i(TAG, addTime(message), e);
	}

	public static void w(String message) {
		w(message, null);
	}

	public static void w(String message, Throwable e) {
		Log.w(TAG, addTime(message), e);
	}

	public static void e(String message) {
		e(message, null);
	}

	public static void e(String message, Throwable e) {
		Log.e(TAG, addTime(message), e);
	}

	private static String addTime(String message) {
		if ( !StringUtil.isValid(message) ) {
			message = "";
		}
		
		builder.setLength(0); // Clean
		builder.append("(");
		builder.append(Calendar.getInstance().getTime());
		builder.append(") ");
		builder.append(message);
		return builder.toString();
	}
	
}

