package br.com.otgmobile.ticketmobileguard.util;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.location.Address;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings.Secure;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

public class AppHelper extends Application{
	
	private static AppHelper instance = null;

	public static AppHelper getInstance() {
		if (instance != null) {
			return instance;
		} else {
			instance = new AppHelper();
			return instance;
		}
	}
	
	

	public void presentError(Context context, String title, String description) {
		AlertDialog.Builder d = new AlertDialog.Builder(context);
		d.setTitle(title);
		d.setMessage(description);
		d.setIcon(android.R.drawable.ic_dialog_alert);
		d.setNeutralButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
			}
		});
		d.show();
	}
	
	


	public String getUniqueDeviceID(Context context) {
		return Secure
				.getString(context.getContentResolver(), Secure.ANDROID_ID);
	}

	public byte[] parseBitmapToByteArray(Bitmap bitmap) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		if (bitmap.compress(CompressFormat.JPEG, 76, bos)) {
			return bos.toByteArray();
		}

		return null;
	}

	@Override
	public void onTerminate() {
		instance = null;
		super.onTerminate();
	}

	public static Long getCurrentTime(){
		return new Date().getTime();
	}

	public String getAddressLineFromGoogleAddress(Address address) {
		String toReturn = "";
		if (address == null) {
			return toReturn;
		}
		for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
			toReturn += address.getAddressLine(i) + " ";
		}
		return toReturn;
	}

	public boolean checkConnection(Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;
		return true;
	}


	public boolean isOnWifi(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
			return true;
		}

		return false;
	}
	
	public void paintTextRed(TextView textView) {
			textView.setTextColor(Color.RED);

	}
	
	public void paintTextRegular(TextView textView){
		textView.setTextColor(Color.BLACK);
		textView.invalidate();
	}
	
	public String getAppVersion(Context context){
		String version = "";
		try {
			 version = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			LogUtil.e("Erro ao pegar a vers?o", e);
		}
		return version;
	}
	
	public static void unbindDrawables(View view) {
        if (view.getBackground() != null) {
        view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup && !(view instanceof AdapterView)) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
            unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
        ((ViewGroup) view).removeAllViews();
        }
    }
	
	

}
