/**
 * 
 */
package br.com.otgmobile.ticketmobileguard.cloud;

import java.sql.SQLException;

import org.apache.http.HttpStatus;

import android.content.Context;
import br.com.otgmobile.ticketmobileguard.database.DatabaseHelper;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;
import br.com.otgmobile.ticketmobileguard.util.Session;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;

/**
 * @author brunoramosdias
 *
 */
public class AcessoCloud extends RestClient {
	
	private static final String ACESSO = "acesso";
	private Context context;
	private Dao<Acesso, Long> acessoDao;
	private static final String PATH ="acessos";
	
	public AcessoCloud(Context context){
		this.context = context;
	}
	
	public void updateEntradas(Acesso entrada) throws Exception{
		 cleanParams();
		 String url = addSlashIfNeeded(Session.getServer(context))+PATH+JSON_TYPE;
		 Gson gson = new Gson();
		 setUrl(url);
		 String jsonString = getJSonStringFromEnttrada(entrada, gson);
		 setToken(Session.getToken(context));
 		 addParam(ACESSO, jsonString);
		 execute(RequestMethod.POST);
		 if(getResponseCode() == HttpStatus.SC_CREATED){
			 updateEntrada(entrada);
		 }
		 LogUtil.d(getErrorMessage());
		 LogUtil.d(Integer.toString(getResponseCode()));
	}

	private String getJSonStringFromEnttrada(Acesso entrada, Gson gson) {
		Ingresso ingresso = entrada.getIngresso();
		if(ingresso != null){
			entrada.setIngressoId(ingresso.getId());
			entrada.setIngresso(null);
		}
		String jsonString = gson.toJson(entrada);
		entrada.setIngresso(ingresso);
		return jsonString;
	}
	
	
	private void updateEntrada(Acesso acesso) throws Exception {
		acesso.setSent(1);
		acessoDao().update(acesso);
		
	}

	private Dao<Acesso, Long> acessoDao(){
		if(acessoDao == null){
			try {
				acessoDao = DatabaseHelper.getDatabase(context).getDao(Acesso.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}		
		return acessoDao;
	}

}
