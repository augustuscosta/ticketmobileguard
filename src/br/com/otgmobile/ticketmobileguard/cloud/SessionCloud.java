package br.com.otgmobile.ticketmobileguard.cloud;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import android.content.Context;

public class SessionCloud extends RestClient{
	
	private static final String PATH = "sessions";
	private Context context;
	
	public SessionCloud(Context context){
		this.context = context;
	}
	
	public void login(String username, String password) throws Exception{
		cleanParams();
		setToken(null);
		String url = br.com.otgmobile.ticketmobileguard.util.Session.getServer(context);
		JSONObject object = new JSONObject();
		object.accumulate("email", username);
		object.accumulate("password", password);
		JSONObject root = new JSONObject();
		root.put("session", object);
		doLogin(url, root);
	}

	private void doLogin(String url, JSONObject root)throws Exception {
		HttpPost httpPost = new HttpPost(addSlashIfNeeded(url)+PATH+JSON_TYPE);
		StringEntity entity = new StringEntity(root.toString(), HTTP.UTF_8);
		entity.setContentType("application/json");
		httpPost.setEntity(entity);
		HttpClient client = new DefaultHttpClient();
		httpResponse = client.execute(httpPost);
		String token = retrieveToken();
		br.com.otgmobile.ticketmobileguard.util.Session.setToken(token, context);
	}

}
