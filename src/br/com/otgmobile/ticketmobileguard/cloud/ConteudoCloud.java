package br.com.otgmobile.ticketmobileguard.cloud;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.ticketmobileguard.database.DatabaseHelper;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;
import br.com.otgmobile.ticketmobileguard.util.Session;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.j256.ormlite.dao.Dao;

public class ConteudoCloud extends RestClient {
	
	private static final String PATH ="empresas/conteudo/all";
	private Context context;
	private Dao<Conteudo, Long> dao;
	
	public ConteudoCloud(Context context){
		this.context = context;
	}
	
	public void getConteudos() throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setToken(Session.getToken(context));
		setUrl(addSlashIfNeeded(url)+PATH);
		execute(RequestMethod.GET);
		getErrorMessage();
		storeResponse(getJsonArrayFromResponse());
	}

	private void storeResponse(JSONArray jsonArray) throws Exception {
		if(jsonArray == null) return;
		
		Conteudo obj;
		Gson gson = new GsonBuilder().create(); 
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject object = jsonArray.getJSONObject(i);
			obj = gson.fromJson(object.getJSONObject("conteudo").toString(), Conteudo.class);
			dao().createOrUpdate(obj);
		}
	}
	

	private Dao<Conteudo, Long> dao(){
		if(dao == null){
			 try {
				dao = DatabaseHelper.getDatabase(context).getDao(Conteudo.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}
		
		return dao;
	}

	
	 
}
