package br.com.otgmobile.ticketmobileguard.cloud;

import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import br.com.otgmobile.ticketmobileguard.database.DatabaseHelper;
import br.com.otgmobile.ticketmobileguard.model.Entrada;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.model.TipoDeIngresso;
import br.com.otgmobile.ticketmobileguard.model.Usuario;
import br.com.otgmobile.ticketmobileguard.util.ConstUtil;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;
import br.com.otgmobile.ticketmobileguard.util.Session;

import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;

/**
 * @author brunoramosdias
 *
 */

public class IngressoCloud extends RestClient {

	private static final String PATH="ingressos/conteudo";
	private Dao<Ingresso,Long> dao;
	private Context context;
	private Dao<TipoDeIngresso, Long> tipoDao;
	private Dao<Usuario, Long> usuarioDao;
	private Dao<Entrada, Long> entradaDao;

	public IngressoCloud(Context context){
		this.context = context;
	}

	public void getIngressos(Long conteudoId) throws Exception{
		cleanParams();
		String url = Session.getServer(context);
		setToken(Session.getToken(context));
		setUrl(addSlashIfNeeded(url)+PATH+ConstUtil.SLASH+conteudoId+JSON_TYPE);
		execute(RequestMethod.GET);
		getErrorMessage();
		storeResponse(getJsonArrayFromResponse());
	}

	private void storeResponse(JSONArray jsonArray) throws Exception {
		Gson gson = new Gson();
		for (int i = 0; i < jsonArray.length(); i++) {
			persistIngressosFromJSon(jsonArray, gson, i);
		}
	}

	private void persistIngressosFromJSon(JSONArray jsonArray, Gson gson, int i)throws JSONException, Exception, SQLException {
		JSONObject object = jsonArray.getJSONObject(i);
		Ingresso	obj = gson.fromJson(object.getString("ingresso").toString(),Ingresso.class);
		if(obj.getResposta() != null && obj.getResposta().length() > 0){
			persistwithAlignedObjects(obj);			
		}
	}

	private void persistwithAlignedObjects(Ingresso obj) throws Exception {
		persistTipo(obj);
		persistUsuario(obj);
		dao().createOrUpdate(obj);
	}

	private void persistTipo(Ingresso obj) throws SQLException {
		if(obj.getTipoDeIngresso()!= null){
			persistEntradas(obj.getTipoDeIngresso());
			tipoDao().createOrUpdate(obj.getTipoDeIngresso());
		}
	}

	private void persistEntradas(TipoDeIngresso tipo) throws SQLException {
		for(Entrada entrada : tipo.getEntradas()){
			entrada.setTipoDeIngresso(tipo);
			entradaDao().createOrUpdate(entrada);
		}
	}

	private void persistUsuario(Ingresso obj) throws SQLException {
		if(obj.getUsuario() != null){
			usuarioDao().createOrUpdate(obj.getUsuario());
		}
	}

	private Dao<Ingresso, Long> dao(){
		if(dao == null){
			try {
				dao = DatabaseHelper.getDatabase(context).getDao(Ingresso.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}		
		return dao;
	}

	private Dao<TipoDeIngresso, Long> tipoDao(){
		if(tipoDao == null){
			try {
				tipoDao = DatabaseHelper.getDatabase(context).getDao(TipoDeIngresso.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}		
		return tipoDao;
	}

	private Dao<Usuario, Long> usuarioDao(){
		if(usuarioDao == null){
			try {
				usuarioDao = DatabaseHelper.getDatabase(context).getDao(Usuario.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}		
		return usuarioDao;
	}

	private Dao<Entrada, Long> entradaDao(){
		if(entradaDao == null){
			try {
				entradaDao = DatabaseHelper.getDatabase(context).getDao(Entrada.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar dao de conteudos", e);
			}
		}		
		return entradaDao;
	}

}
