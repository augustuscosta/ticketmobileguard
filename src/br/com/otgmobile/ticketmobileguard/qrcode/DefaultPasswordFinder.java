package br.com.otgmobile.ticketmobileguard.qrcode;

import org.bouncycastle.openssl.PasswordFinder;

public class DefaultPasswordFinder implements PasswordFinder{

	private char[] password;
	
	public DefaultPasswordFinder(char[] password) {
		this.password = password;
	}
	
	@Override
	public char[] getPassword() {
		return password;
	}

}
