package br.com.otgmobile.ticketmobileguard.qrcode;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.security.KeyPair;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.openssl.PEMReader;

import android.content.Context;
import android.util.Base64;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

import com.google.gson.Gson;

@SuppressWarnings("deprecation")
public class Decripter {

	private static final String AES = "AES";
	private static final String KEY_FINALIZER = "#OT1#";
	private static final String KEY_INITIALIZER = "#OTG1#";
	private static final String RSA = "RSA";
	private static final String AES_CBC_PKCS5_PADDING = "AES/CBC/PKCS5Padding";
	private static final String UTF8 = "UTF8";
	private static final String PRIVATE_PASSWORD = "1q2w3e";
	private static final String CIPHERIV="OTGcypherivkeyOTG";

	public static String decodeBase64(String value) {
		if(value == null){
			return null;
		}

		byte[] responseBytes= Base64.decode(value, Base64.DEFAULT);
		String decodedResponse = new String(responseBytes);
		return decodedResponse;
	}

	public static Ingresso fromJSonDecoded(final String encoded, final Context context){	
		String ingressojson;
		try {
			ingressojson = getJSonWithDEcription(encoded, context);
			String decoded = decodeBase64(ingressojson);
			return new Gson().fromJson(decoded, Ingresso.class);
		} catch (Exception e) {
			LogUtil.e("erro ao decodificar o ingresso", e);
			return null;
		}
	}

	private static String getJSonWithDEcription(String encoded, Context context){
		if(encoded == null || encoded.length() < 1 || encoded.lastIndexOf(KEY_INITIALIZER) == -1){
			return null;
		}
		
		String encriptedKey = encoded.substring(encoded.indexOf(KEY_INITIALIZER),encoded.indexOf(KEY_FINALIZER));
		encriptedKey = encriptedKey.replace("#OTG1#", "");
		byte[] decodeKey = decodeWithPrivateKeyBytes(context, encriptedKey);
		String finalText = encoded.substring(encoded.lastIndexOf(KEY_FINALIZER));
		Cipher aes = null;
		try {
			aes = Cipher.getInstance(AES_CBC_PKCS5_PADDING);
			aes.init(Cipher.DECRYPT_MODE, new SecretKeySpec(decodeKey, AES), new IvParameterSpec(CIPHERIV.getBytes(), 0, aes.getBlockSize()));
			byte[] utf8 = aes.doFinal(finalText.getBytes());
			return new String(utf8,UTF8);			
		} catch (Exception e) {
			LogUtil.e("erro ao ler as chaves do cipher", e);
			return null;
		}
	}

	public static String decodeWithPrivateKey( final Context context, final String encoded){
		try {
			byte[] buffer = decodeBase64(encoded).getBytes();
			Cipher rsa;
			rsa = Cipher.getInstance(RSA);
			rsa.init(Cipher.DECRYPT_MODE, readKeyPair(PRIVATE_PASSWORD, context).getPrivate());
			byte[] utf8 = rsa.doFinal(buffer);
			return new String(utf8, UTF8);
		} catch (Exception e) {
			LogUtil.e("erro ao decriptat ingresso",e);
		}
		return null;
	}
	
	public static byte[] decodeWithPrivateKeyBytes( final Context context, final String encoded){
		try {
			byte[] buffer = encoded.getBytes();
			Cipher rsa;
			rsa = Cipher.getInstance(RSA);
			rsa.init(Cipher.DECRYPT_MODE, readKeyPair(PRIVATE_PASSWORD, context).getPrivate());
			byte[] utf8 = rsa.doFinal(buffer);
			return utf8;
		} catch (Exception e) {
			LogUtil.e("erro ao decriptat ingresso",e);
		}
		return null;
	}

	private static KeyPair readKeyPair(String keyPassword, final Context context) throws IOException {
		InputStream inputStream = context.getResources().openRawResource(R.raw.decript);
		Reader reader = new InputStreamReader(inputStream);
		PEMReader r = new PEMReader(reader,new DefaultPasswordFinder(keyPassword.toCharArray()));
		try {
			return (KeyPair) r.readObject();
		} catch (IOException ex) {
			throw ex;
		} finally {
			r.close();
			reader.close();
		}
	}

}
