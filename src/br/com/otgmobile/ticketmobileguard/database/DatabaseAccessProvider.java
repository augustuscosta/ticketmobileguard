package br.com.otgmobile.ticketmobileguard.database;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Entrada;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.model.Usuario;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;

public class DatabaseAccessProvider {
	
	/**
	 * 
	 */
	private DatabaseHelper databaseHelper;
	private Dao<Conteudo, Long> conteudoDao;
	private Dao<Ingresso, Long> ingressoDao;
	private Dao<Usuario, Long> usuarioDao;
	private Dao<Entrada,Long> entradaDao;
	private Dao<Acesso,Long> acessoDao;
	private static final String NOME ="name";
	private static final String CPF ="cpf";
	private static final String RG ="rg";
	
	public DatabaseAccessProvider(Context context){
		databaseHelper = (DatabaseHelper) DatabaseHelper.getDatabase(context);
	}
	
				
	//DAO Lazy Instantiantions
	
	public Dao<Conteudo, Long> conteudoDao(){
		if(conteudoDao == null){
			try {
				conteudoDao = databaseHelper.getDao(Conteudo.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}
		
		return conteudoDao;
		
	}
	
	
	public Dao<Ingresso, Long> ingressoDao(){ 
		if(ingressoDao == null){
			try {
				ingressoDao = databaseHelper.getDao(Ingresso.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao", e);
			}
		}
		
		return ingressoDao;
	}
	
	public Dao<Usuario, Long> usuarioDao(){
		if(usuarioDao == null){
			try {
				usuarioDao = databaseHelper.getDao(Usuario.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}
		
		return usuarioDao;
	}
	
	public Dao<Entrada, Long> entradaDao(){
		if(entradaDao == null){
			try {
				entradaDao = databaseHelper.getDao(Entrada.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}
		
		return entradaDao;
	}
	
	public Dao<Acesso, Long> acessoDao(){
		if(acessoDao == null){
			try {
				acessoDao = databaseHelper.getDao(Acesso.class);
			} catch (SQLException e) {
				LogUtil.e("erro ao instanciar o dao",e);
			}
		}
		
		return acessoDao;
	}
	
	//Get Conteudos
	
	public List<Conteudo> getAllEventos(){
		try {
			QueryBuilder<Conteudo, Long> builder = conteudoDao().queryBuilder();
			builder.where().isNull("parentId");
			List<Conteudo> conteudos = builder.query();
			return conteudos;
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				conteudoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	


	public List<Conteudo> getAllConteudoByCriteria(String criteria){
		QueryBuilder<Conteudo, Long> builder = conteudoDao().queryBuilder();
		try {
			builder.where().like("id", "'%"+ criteria+"%'")
			.or().like("titulo", "'%"+ criteria+"%'").or().like("description", "'%"+ criteria+"%'");
			return builder.query();
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				conteudoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	public Conteudo getConteudos(Long id){
		try {
			Conteudo conteudo = conteudoDao().queryForId(id);
			return conteudo;
		} catch (SQLException e) {
			LogUtil.e("erro ao pegar evento " +id, e);
			return null;
		}finally{
			try {
				conteudoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	
	// Get Ingressos
	
	public List<Ingresso> getAllIngressos(){
		try {
			return ingressoDao().queryForAll();
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				ingressoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	public Ingresso getIngresso(Long id){
		try {
			return ingressoDao().queryForId(id);
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				ingressoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	public Ingresso getIngresso(String code){
		try {
			List<Ingresso> ingressos = ingressoDao().queryForEq("resposta", code);
			if(ingressos != null && !ingressos.isEmpty()){
				return ingressoDao().queryForEq("resposta", code).get(0);				
			}else {
				return null;
			}
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				ingressoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	public List<Ingresso> getIngressoFromEvent(Long id){
		try {
			List<Ingresso> ingressos = ingressoDao().queryForEq("conteudoId", id);
			return addDataToIngressos(ingressos);
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				ingressoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}
	
	public List<Ingresso> getAllIngressoByCriteriaAndConteudo(String criteria,Long id){
		QueryBuilder<Ingresso, Long> builder = ingressoDao().queryBuilder();
		QueryBuilder<Usuario, Long> uBuilder = usuarioDao().queryBuilder();
		SelectArg criteriaSelectArg = new SelectArg();
		try {
			uBuilder.where().like(NOME, "'%"+ criteriaSelectArg+"%'").or().like(CPF, "'%"+ criteriaSelectArg+"%'")
			.or().like(RG, "'%"+ criteriaSelectArg+"%'");
			criteriaSelectArg.setValue(criteria);
			 builder.where().eq("conteudoId", id).and().in("usuario", uBuilder);
			return builder.query();
		} catch (SQLException e) {
			LogUtil.e("erro ao listar eventos", e);
			return null;
		}finally{
			try {
				ingressoDao().closeLastIterator();
			} catch (SQLException e) {
				LogUtil.e("fechar cursor", e);
			}
		}
	}


	private List<Ingresso> addDataToIngressos(List<Ingresso> ingressos) {
		if(ingressos == null || ingressos.isEmpty()){
			return null;			
		}
		
		for (Ingresso ingresso : ingressos) {
			ingresso = addDataToIngresso(ingresso);
		}
		
		return ingressos;
	}


	private Ingresso addDataToIngresso(Ingresso ingresso) {
		if(ingresso == null){
			return null;			
		}
		
		ingresso.setConteudo(getConteudos(ingresso.getConteudoId()));
		return ingresso;
	}

   public void clearObjectCahe(){
	   try {
		conteudoDao().deleteBuilder().delete();
		ingressoDao().deleteBuilder().delete();
		usuarioDao().deleteBuilder().delete();
		entradaDao().deleteBuilder().delete();
		acessoDao().deleteBuilder().delete();
	} catch (SQLException e) {
		LogUtil.e("erro ao deletar os conteudos", e);
	}
   }

   
   public List<Acesso> getUnsentAcessos(){
	   try {
		return acessoDao().queryForEq("sent", 0);
	} catch (SQLException e) {
		LogUtil.e("erro ao pergar acessos do banco", e);
		return null;
	}finally{
		try {
			acessoDao().closeLastIterator();
		} catch (SQLException e) {
			LogUtil.e("erro ao fechar cursor do objeto acesso", e);
		}
	}
	   
   }
   
   public List<Acesso> getAcessosFromIngresso(Long id){
	   try {
		   return acessoDao().queryForEq("ingresso", id);
	   } catch (SQLException e) {
		   LogUtil.e("erro ao pergar acessos do banco", e);
		   return null;
	   }finally{
		   try {
			   acessoDao().closeLastIterator();
		   } catch (SQLException e) {
			   LogUtil.e("erro ao fechar cursor do objeto acesso", e);
		   }
	   }
	   
   }
   

}
