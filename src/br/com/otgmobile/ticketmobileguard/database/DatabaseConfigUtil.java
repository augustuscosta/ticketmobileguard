package br.com.otgmobile.ticketmobileguard.database;


import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.model.Entrada;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.model.TipoDeIngresso;
import br.com.otgmobile.ticketmobileguard.model.Usuario;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {

	private static final Class<?>[] classes = new Class[] {
		Conteudo.class,Ingresso.class,TipoDeIngresso.class,Usuario.class,
		Entrada.class,Acesso.class
	};

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	} 
}