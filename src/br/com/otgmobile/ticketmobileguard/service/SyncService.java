/**
 * 
 */
package br.com.otgmobile.ticketmobileguard.service;

import java.util.List;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import br.com.otgmobile.ticketmobileguard.cloud.AcessoCloud;
import br.com.otgmobile.ticketmobileguard.database.DatabaseAccessProvider;
import br.com.otgmobile.ticketmobileguard.model.Acesso;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

/**
 * @author Bruno Ramos Dias
 *
 */
public class SyncService extends Service {
	
	public static final String SYNC_SERVICE_INTENT = "SYNC_SERVICE_INTENT";
	private SyncServiceBinder mBinder = new SyncServiceBinder();
	public static final int SLEEP_TIME = 30000;
	private AcessoCloud cloud;
	private DatabaseAccessProvider provider;
	private boolean isRunning;
	
	public static void startService(Context context){
		context.startService(new Intent(SYNC_SERVICE_INTENT));
	}
	
	public static void stopService(Context context){
		context.stopService(new Intent(SYNC_SERVICE_INTENT));
	}
	

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}
	
	public class SyncServiceBinder extends Binder{
		
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		LogUtil.initLog(this, "TICKET_MOBILE_GUARD");
		startSending();
		return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy() {
		isRunning = false;
		super.onDestroy();
	}

	private void startSending() {
		if(isRunning){
			LogUtil.d("sender is running");
		}
		
		Thread thread = new Thread(mSyncRunnable);
		thread.setPriority(Thread.MIN_PRIORITY);
		thread.start();
		isRunning = true;
	}
	
	@SuppressWarnings("static-access")
	final private Thread mSyncRunnable = new Thread() {
		
		@Override
		public void run() {
			
			while ( isRunning ) {
				List<Acesso> acessosPorEnviar = provider().getUnsentAcessos();
				if(acessosPorEnviar != null && !acessosPorEnviar.isEmpty()){
					LogUtil.d("enviaando acessos para o servidor" + acessosPorEnviar.size());
					for (int i = 0; i < acessosPorEnviar.size(); i++) {	
						try {
							LogUtil.d("enviaando acessos"+i+1+" para o servidor");				
							cloud().updateEntradas(acessosPorEnviar.get(i));
						} catch (Exception e) {
							LogUtil.e("erro ao enviar entrada para o servidor", e);
						}
					}
				}
				try {
					this.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					LogUtil.e("erro do por a thread em modo e espera", e);
					isRunning = false;
				}
			}
			
		}
	};
	
	private DatabaseAccessProvider provider(){
		if(provider == null){
			provider = new  DatabaseAccessProvider(this);
		}
		
		return provider;
	}
	
	private AcessoCloud cloud(){
		if(cloud == null){
			cloud = new AcessoCloud(this);
		}
		return cloud;
	}

}
