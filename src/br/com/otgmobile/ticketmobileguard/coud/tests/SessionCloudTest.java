package br.com.otgmobile.ticketmobileguard.coud.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.ticketmobileguard.cloud.SessionCloud;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

public class SessionCloudTest extends AndroidTestCase {
	
	@Override
	protected void setUp() throws Exception {
		LogUtil.initLog(getContext(), "TICKET_MOBILE_GUARD_TEST");
		super.setUp();
	}
	
	public void testSessionAtuh() throws Exception{
		String username ="administrador@otgmobile.com.br";
		String password ="1q2w3e";
		SessionCloud cloud = new SessionCloud(getContext());
		cloud.login(username, password);
		Assert.assertTrue(cloud.retrieveToken() != null);
	}

}
