package br.com.otgmobile.ticketmobileguard.coud.tests;

import java.util.List;

import junit.framework.Assert;
import android.test.AndroidTestCase;
import br.com.otgmobile.ticketmobileguard.cloud.ConteudoCloud;
import br.com.otgmobile.ticketmobileguard.cloud.SessionCloud;
import br.com.otgmobile.ticketmobileguard.database.DatabaseHelper;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.util.LogUtil;

public class FullCloudTest extends AndroidTestCase {
	
	@Override
	protected void setUp() throws Exception {
		LogUtil.initLog(getContext(), "TICKET_MOBILE_GUARD_TEST");
		String username ="administrador@otgmobile.com.br";
		String password ="1q2w3e";
		SessionCloud cloud = new SessionCloud(getContext());
		cloud.login(username, password);
		super.setUp();
	}
	
	public void testGetConteudos() throws Exception{
		 ConteudoCloud cloud = new ConteudoCloud(getContext());
		 cloud.getConteudos();
		 List<Conteudo> conteudos = DatabaseHelper.getDatabase(getContext()).getDao(Conteudo.class).queryForAll();
		 Assert.assertNotNull(conteudos);
		 Assert.assertTrue(!conteudos.isEmpty());
	}
	

}
