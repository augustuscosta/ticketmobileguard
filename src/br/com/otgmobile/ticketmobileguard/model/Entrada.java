package br.com.otgmobile.ticketmobileguard.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author brunoramosdias
 *
 */

@DatabaseTable
public class Entrada  implements Serializable{

	private static final long serialVersionUID = 7109797469443853747L;

	@DatabaseField(id=true)
	private Long id;

	@DatabaseField
	private Date data;

	@DatabaseField
	private Long quatidade;

	@DatabaseField
	private boolean checked;

	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
	private TipoDeIngresso tipoDeIngresso;


	@ForeignCollectionField(eager=true)
	private Collection<Acesso> acessos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Long getQuatidade() {
		return quatidade;
	}

	public void setQuatidade(Long quatidade) {
		this.quatidade = quatidade;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public TipoDeIngresso getTipoDeIngresso() {
		return tipoDeIngresso;
	}

	public void setTipoDeIngresso(TipoDeIngresso tipoDeIngresso) {
		this.tipoDeIngresso = tipoDeIngresso;
	}


	public Collection<Acesso> getAcessos() {
		return acessos;
	}

	public void setAcessos(Collection<Acesso> acessos) {
		this.acessos = acessos;
	}

}
