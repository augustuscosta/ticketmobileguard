/**
 * 
 */
package br.com.otgmobile.ticketmobileguard.model;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author brunoramosdias
 *
 */

@DatabaseTable
public class Acesso implements Serializable{
	
	private static final long serialVersionUID = -7721333877485574279L;

	@DatabaseField(generatedId=true)
	private Long id;
	
	@DatabaseField
	@SerializedName("ingresso_id")
	private Long ingressoId;
	
	@DatabaseField
	@SerializedName("entrada_id")
	private Long entradaId;
	
	@DatabaseField(foreign =true)
	private Ingresso ingresso;
	
	@DatabaseField(foreign =true)
	private Entrada entrada;
	
	@DatabaseField
	private Integer sent;
	
	@DatabaseField
	private Date data;

	public Integer getSent() {
		return sent;
	}

	public void setSent(Integer sent) {
		this.sent = sent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIngressoId() {
		return ingressoId;
	}

	public void setIngressoId(Long ingressoId) {
		this.ingressoId = ingressoId;
	}

	public Long getEntradaId() {
		return entradaId;
	}

	public void setEntradaId(Long entradaId) {
		this.entradaId = entradaId;
	}

	public Ingresso getIngresso() {
		return ingresso;
	}

	public void setIngresso(Ingresso ingresso) {
		this.ingresso = ingresso;
	}

	public Entrada getEntrada() {
		return entrada;
	}

	public void setEntrada(Entrada entrada) {
		this.entrada = entrada;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}


}
