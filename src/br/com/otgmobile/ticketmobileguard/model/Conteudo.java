package br.com.otgmobile.ticketmobileguard.model;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author brunoramosdias
 *
 */

@DatabaseTable(tableName="conteudo")
public class Conteudo  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2233903272451531740L;

	@DatabaseField(id = true)
	@SerializedName("id")
	private Long id;

	@DatabaseField
	@SerializedName("parent_id")
	private Long parentId;

	@DatabaseField
	@SerializedName("created_at")
	private Date createdAt;

	@DatabaseField
	@SerializedName("updated_at")
	private Date updatedAt;

	@DatabaseField	
	@SerializedName("categoria")
	private String category;

	@DatabaseField
	@SerializedName("descricao")
	private String description;

	@DatabaseField
	private String titulo;

	@DatabaseField
	private String subtitulo;

	@DatabaseField
	@SerializedName("imagem_content_type")
	private String imagemContentType;

	@DatabaseField
	@SerializedName("imagem_url")
	private String imagemUrl;

	@DatabaseField
	@SerializedName("imagem_url")
	private String imagemThumbUrl;

	@DatabaseField
	@SerializedName("imagem_medium_url")
	private String imagemMediumUrl;

	@DatabaseField
	@SerializedName("imagem_large_url")
	private String imagemLargeUrl;

	@DatabaseField
	@SerializedName("imagem_Icon_url")
	private String imagemIconUrl;

	@DatabaseField
	@SerializedName("row_imagem_file_size")
	private Long imagemFileSize;

	@DatabaseField
	@SerializedName("row_imagem_updated_at")
	private Date imagemUpdatedAt;

	@DatabaseField
	@SerializedName("row_imagem_url")
	private String rowImagemUrl;

	@DatabaseField
	@SerializedName("row_imagem_url")
	private String rowImagemThumbUrl;

	@DatabaseField
	@SerializedName("row_imagem_medium_url")
	private String rowImagemMediumUrl;

	@DatabaseField
	@SerializedName("row_imagem_large_url")
	private String rowImagemLargeUrl;	

	@DatabaseField
	private Double latitude;

	@DatabaseField
	private Double longitude;

	@DatabaseField
	private Boolean social;

	@DatabaseField
	private Boolean ativo;

	@DatabaseField
	private String endereco;

	@DatabaseField
	private String url;
	
	@DatabaseField
	private Date data;

	private List<Ingresso> ingressos;


	private List<TipoDeIngresso> tiposdeIngresso;

	public Conteudo(){
		// To use ORMLite5
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public Long getParentId() {
		return parentId;
	}


	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}


	public Date getCreatedAt() {
		return createdAt;
	}


	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public String getSubtitulo() {
		return subtitulo;
	}


	public void setSubtitulo(String subtitulo) {
		this.subtitulo = subtitulo;
	}


	public String getImagemContentType() {
		return imagemContentType;
	}


	public void setImagemContentType(String imagemContentType) {
		this.imagemContentType = imagemContentType;
	}


	public String getImagemUrl() {
		return imagemUrl;
	}


	public void setImagemUrl(String imagemUrl) {
		this.imagemUrl = imagemUrl;
	}


	public String getImagemThumbUrl() {
		return imagemThumbUrl;
	}


	public void setImagemThumbUrl(String imagemThumbUrl) {
		this.imagemThumbUrl = imagemThumbUrl;
	}


	public String getImagemMediumUrl() {
		return imagemMediumUrl;
	}


	public void setImagemMediumUrl(String imagemMediumUrl) {
		this.imagemMediumUrl = imagemMediumUrl;
	}


	public String getImagemLargeUrl() {
		return imagemLargeUrl;
	}


	public void setImagemLargeUrl(String imagemLargeUrl) {
		this.imagemLargeUrl = imagemLargeUrl;
	}


	public String getImagemIconUrl() {
		return imagemIconUrl;
	}


	public void setImagemIconUrl(String imagemIconUrl) {
		this.imagemIconUrl = imagemIconUrl;
	}


	public Long getImagemFileSize() {
		return imagemFileSize;
	}


	public void setImagemFileSize(Long imagemFileSize) {
		this.imagemFileSize = imagemFileSize;
	}


	public Date getImagemUpdatedAt() {
		return imagemUpdatedAt;
	}

	public void setImagemUpdatedAt(Date imagemUpdatedAt) {
		this.imagemUpdatedAt = imagemUpdatedAt;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}


	public String getRowImagemUrl() {
		return rowImagemUrl;
	}


	public void setRowImagemUrl(String rowImagemUrl) {
		this.rowImagemUrl = rowImagemUrl;
	}


	public String getRowImagemThumbUrl() {
		return rowImagemThumbUrl;
	}


	public void setRowImagemThumbUrl(String rowImagemThumbUrl) {
		this.rowImagemThumbUrl = rowImagemThumbUrl;
	}


	public String getRowImagemMediumUrl() {
		return rowImagemMediumUrl;
	}


	public void setRowImagemMediumUrl(String rowImagemMediumUrl) {
		this.rowImagemMediumUrl = rowImagemMediumUrl;
	}


	public String getRowImagemLargeUrl() {
		return rowImagemLargeUrl;
	}


	public void setRowImagemLargeUrl(String rowImagemLargeUrl) {
		this.rowImagemLargeUrl = rowImagemLargeUrl;
	}


	public Double getLatitude() {
		return latitude;
	}


	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}


	public Double getLongitude() {
		return longitude;
	}


	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}


	public Boolean getSocial() {
		return social;
	}


	public void setSocial(Boolean social) {
		this.social = social;
	}


	public Boolean getAtivo() {
		return ativo;
	}


	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public List<Ingresso> getIngressos() {
		return ingressos;
	}

	public void setIngressos(List<Ingresso> ingressos) {
		this.ingressos = ingressos;
	}

	public List<TipoDeIngresso> getTiposdeIngresso() {
		return tiposdeIngresso;
	}


	public void setTiposdeIngresso(List<TipoDeIngresso> tiposdeIngresso) {
		this.tiposdeIngresso = tiposdeIngresso;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ativo == null) ? 0 : ativo.hashCode());
		result = prime * result
				+ ((category == null) ? 0 : category.hashCode());
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((endereco == null) ? 0 : endereco.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((imagemContentType == null) ? 0 : imagemContentType
						.hashCode());
		result = prime * result
				+ ((imagemFileSize == null) ? 0 : imagemFileSize.hashCode());
		result = prime * result
				+ ((imagemIconUrl == null) ? 0 : imagemIconUrl.hashCode());
		result = prime * result
				+ ((imagemLargeUrl == null) ? 0 : imagemLargeUrl.hashCode());
		result = prime * result
				+ ((imagemMediumUrl == null) ? 0 : imagemMediumUrl.hashCode());
		result = prime * result
				+ ((imagemThumbUrl == null) ? 0 : imagemThumbUrl.hashCode());
		result = prime * result
				+ ((imagemUpdatedAt == null) ? 0 : imagemUpdatedAt.hashCode());
		result = prime * result
				+ ((imagemUrl == null) ? 0 : imagemUrl.hashCode());
		result = prime * result
				+ ((ingressos == null) ? 0 : ingressos.hashCode());
		result = prime * result
				+ ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result
				+ ((longitude == null) ? 0 : longitude.hashCode());
		result = prime * result
				+ ((parentId == null) ? 0 : parentId.hashCode());
		result = prime
				* result
				+ ((rowImagemLargeUrl == null) ? 0 : rowImagemLargeUrl
						.hashCode());
		result = prime
				* result
				+ ((rowImagemMediumUrl == null) ? 0 : rowImagemMediumUrl
						.hashCode());
		result = prime
				* result
				+ ((rowImagemThumbUrl == null) ? 0 : rowImagemThumbUrl
						.hashCode());
		result = prime * result
				+ ((rowImagemUrl == null) ? 0 : rowImagemUrl.hashCode());
		result = prime * result + ((social == null) ? 0 : social.hashCode());
		result = prime * result
				+ ((subtitulo == null) ? 0 : subtitulo.hashCode());
		result = prime * result
				+ ((tiposdeIngresso == null) ? 0 : tiposdeIngresso.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		result = prime * result
				+ ((updatedAt == null) ? 0 : updatedAt.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Conteudo other = (Conteudo) obj;
		if (ativo == null) {
			if (other.ativo != null)
				return false;
		} else if (!ativo.equals(other.ativo))
			return false;
		if (category == null) {
			if (other.category != null)
				return false;
		} else if (!category.equals(other.category))
			return false;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (endereco == null) {
			if (other.endereco != null)
				return false;
		} else if (!endereco.equals(other.endereco))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (imagemContentType == null) {
			if (other.imagemContentType != null)
				return false;
		} else if (!imagemContentType.equals(other.imagemContentType))
			return false;
		if (imagemFileSize == null) {
			if (other.imagemFileSize != null)
				return false;
		} else if (!imagemFileSize.equals(other.imagemFileSize))
			return false;
		if (imagemIconUrl == null) {
			if (other.imagemIconUrl != null)
				return false;
		} else if (!imagemIconUrl.equals(other.imagemIconUrl))
			return false;
		if (imagemLargeUrl == null) {
			if (other.imagemLargeUrl != null)
				return false;
		} else if (!imagemLargeUrl.equals(other.imagemLargeUrl))
			return false;
		if (imagemMediumUrl == null) {
			if (other.imagemMediumUrl != null)
				return false;
		} else if (!imagemMediumUrl.equals(other.imagemMediumUrl))
			return false;
		if (imagemThumbUrl == null) {
			if (other.imagemThumbUrl != null)
				return false;
		} else if (!imagemThumbUrl.equals(other.imagemThumbUrl))
			return false;
		if (imagemUpdatedAt == null) {
			if (other.imagemUpdatedAt != null)
				return false;
		} else if (!imagemUpdatedAt.equals(other.imagemUpdatedAt))
			return false;
		if (imagemUrl == null) {
			if (other.imagemUrl != null)
				return false;
		} else if (!imagemUrl.equals(other.imagemUrl))
			return false;
		if (ingressos == null) {
			if (other.ingressos != null)
				return false;
		} else if (!ingressos.equals(other.ingressos))
			return false;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (rowImagemLargeUrl == null) {
			if (other.rowImagemLargeUrl != null)
				return false;
		} else if (!rowImagemLargeUrl.equals(other.rowImagemLargeUrl))
			return false;
		if (rowImagemMediumUrl == null) {
			if (other.rowImagemMediumUrl != null)
				return false;
		} else if (!rowImagemMediumUrl.equals(other.rowImagemMediumUrl))
			return false;
		if (rowImagemThumbUrl == null) {
			if (other.rowImagemThumbUrl != null)
				return false;
		} else if (!rowImagemThumbUrl.equals(other.rowImagemThumbUrl))
			return false;
		if (rowImagemUrl == null) {
			if (other.rowImagemUrl != null)
				return false;
		} else if (!rowImagemUrl.equals(other.rowImagemUrl))
			return false;
		if (social == null) {
			if (other.social != null)
				return false;
		} else if (!social.equals(other.social))
			return false;
		if (subtitulo == null) {
			if (other.subtitulo != null)
				return false;
		} else if (!subtitulo.equals(other.subtitulo))
			return false;
		if (tiposdeIngresso == null) {
			if (other.tiposdeIngresso != null)
				return false;
		} else if (!tiposdeIngresso.equals(other.tiposdeIngresso))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		if (updatedAt == null) {
			if (other.updatedAt != null)
				return false;
		} else if (!updatedAt.equals(other.updatedAt))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}


	
}
