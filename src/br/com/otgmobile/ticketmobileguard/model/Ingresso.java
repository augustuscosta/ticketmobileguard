package br.com.otgmobile.ticketmobileguard.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author brunoramosdias
 *
 */

@DatabaseTable
public class Ingresso implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5146133266608624456L;

	@DatabaseField(id=true)
	private Long id;
	
	@DatabaseField()
	@SerializedName("usuario_id")
	private Long usuarioId;
	
	@DatabaseField()
	@SerializedName("conteudo_id")
	private Long conteudoId;
	
	@DatabaseField()
	@SerializedName("tipo_de_ingresso_id")
	private Long tipoDeIngressoId;
	
	@DatabaseField()
	@SerializedName("created_at")
	private Date createdAt;
	
	@DatabaseField()
	private boolean autentichated;
	
	@DatabaseField()
	private String resposta;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true,columnName="usuario")
	private Usuario usuario;
	
	private Conteudo conteudo;
	
	@ForeignCollectionField(eager=true)
	private Collection<Acesso> acessos;

	
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
	@SerializedName("tipo_de_ingresso")
	private TipoDeIngresso tipoDeIngresso;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Long usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getConteudoId() {
		return conteudoId;
	}

	public void setConteudoId(Long conteudoId) {
		this.conteudoId = conteudoId;
	}

	public Long getTipoDeIngressoId() {
		return tipoDeIngressoId;
	}

	public void setTipoDeIngressoId(Long tipoDeIngressoId) {
		this.tipoDeIngressoId = tipoDeIngressoId;
	}

	public boolean isAutentichated() {
		return autentichated;
	}

	public void setAutentichated(boolean autentichated) {
		this.autentichated = autentichated;
	}

	public String getResposta() {
		return resposta;
	}

	public void setResposta(String resposta) {
		this.resposta = resposta;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Conteudo getConteudo() {
		return conteudo;
	}

	public void setConteudo(Conteudo conteudo) {
		this.conteudo = conteudo;
	}

	public TipoDeIngresso getTipoDeIngresso() {
		return tipoDeIngresso;
	}

	public void setTipoDeIngresso(TipoDeIngresso tipoDeIngresso) {
		this.tipoDeIngresso = tipoDeIngresso;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}



	public Collection<Acesso> getAcessos() {
		return acessos;
	}

	public void setAcessos(Collection<Acesso> acessos) {
		this.acessos = acessos;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((conteudoId == null) ? 0 : conteudoId.hashCode());
		result = prime * result
				+ ((createdAt == null) ? 0 : createdAt.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((tipoDeIngressoId == null) ? 0 : tipoDeIngressoId.hashCode());
		result = prime * result
				+ ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ingresso other = (Ingresso) obj;
		if (conteudoId == null) {
			if (other.conteudoId != null)
				return false;
		} else if (!conteudoId.equals(other.conteudoId))
			return false;
		if (createdAt == null) {
			if (other.createdAt != null)
				return false;
		} else if (!createdAt.equals(other.createdAt))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (tipoDeIngressoId == null) {
			if (other.tipoDeIngressoId != null)
				return false;
		} else if (!tipoDeIngressoId.equals(other.tipoDeIngressoId))
			return false;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}

	
	

}
