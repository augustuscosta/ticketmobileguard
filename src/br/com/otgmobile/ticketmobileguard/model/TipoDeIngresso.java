package br.com.otgmobile.ticketmobileguard.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author brunoramosdias
 *
 */


@DatabaseTable
public class TipoDeIngresso implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5808814103216444199L;

	@DatabaseField(id=true)
	private Long  id;

	@DatabaseField
	private String  descricao;
	
	@DatabaseField
	private String  empresaId;
	
	@DatabaseField
	private Long  quantidadeIngressos;
	
	@DatabaseField
	private String  titulo;
	
	@DatabaseField()
	private Long conteudoId;
	
	private Conteudo conteudo;
	
	@DatabaseField()
	private Long ingressoId;
	
	@DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
	private Ingresso ingresso;
	
	@DatabaseField
	private Double valor;
	
	@DatabaseField
	private Date  createdAt;
	
	@DatabaseField
	private Date updatedAt;
	
	@ForeignCollectionField
	private Collection<Ingresso> ingressos;
	
	@ForeignCollectionField(eager=true)
	private Collection<Entrada> entradas;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEmpresaId() {
		return empresaId;
	}

	public void setEmpresaId(String empresaId) {
		this.empresaId = empresaId;
	}

	public Long getQuantidadeIngressos() {
		return quantidadeIngressos;
	}

	public void setQuantidadeIngressos(Long quantidadeIngressos) {
		this.quantidadeIngressos = quantidadeIngressos;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Conteudo getConteudo() {
		return conteudo;
	}

	public void setConteudo(Conteudo conteudo) {
		this.conteudo = conteudo;
	}

	public Ingresso getIngresso() {
		return ingresso;
	}

	public void setIngresso(Ingresso ingresso) {
		this.ingresso = ingresso;
	}

	public Long getConteudoId() {
		return conteudoId;
	}

	public void setConteudoId(Long conteudoId) {
		this.conteudoId = conteudoId;
	}

	public Long getIngressoId() {
		return ingressoId;
	}

	public void setIngressoId(Long ingressoId) {
		this.ingressoId = ingressoId;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public Collection<Ingresso> getIngressos() {
		return ingressos;
	}

	public void setIngressos(Collection<Ingresso> ingressos) {
		this.ingressos = ingressos;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public Collection<Entrada> getEntradas() {
		return entradas;
	}

	public void setEntradas(Collection<Entrada> entradas) {
		this.entradas = entradas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((conteudoId == null) ? 0 : conteudoId.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoDeIngresso other = (TipoDeIngresso) obj;
		if (conteudoId == null) {
			if (other.conteudoId != null)
				return false;
		} else if (!conteudoId.equals(other.conteudoId))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}
	
	

}
