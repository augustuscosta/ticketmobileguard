package br.com.otgmobile.ticketmobileguard.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.model.Ingresso;
import br.com.otgmobile.ticketmobileguard.row.IngressoRowHolderInfo;

public class IngressoAdapter extends BaseAdapter {
	
	private List<Ingresso> ingressos;
	private Activity context;
	
	public IngressoAdapter(List<Ingresso> ingressos, Activity context){
		this.ingressos = ingressos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return ingressos == null? 0: ingressos.size();
	}

	@Override
	public Ingresso getItem(int position) {
		return ingressos == null? null : ingressos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		IngressoRowHolderInfo holder;
		Ingresso obj = ingressos.get(position);
		if(convertView == null){
			holder = new IngressoRowHolderInfo();
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.ingresso_row, null);
			holder.tipo = (TextView) row.findViewById(R.id.tipo);
			holder.valor = (TextView) row.findViewById(R.id.valor);
			holder.comprador = (TextView) row.findViewById(R.id.compradorLabel);
			holder.tipoLabel = (TextView) row.findViewById(R.id.tipoLabel);
			holder.valorLabel = (TextView) row.findViewById(R.id.valorLabel);
			holder.compradorLabel = (TextView) row.findViewById(R.id.compradorLabel);
			
		}else{
			row =  convertView;
			holder = (IngressoRowHolderInfo) row.getTag();
		}
		holder.drawRow(obj);
		row.setTag(holder);
		
		return row;
	}

}
