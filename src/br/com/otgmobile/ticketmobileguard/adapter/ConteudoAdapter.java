package br.com.otgmobile.ticketmobileguard.adapter;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import br.com.otgmobile.ticketmobileguard.R;
import br.com.otgmobile.ticketmobileguard.model.Conteudo;
import br.com.otgmobile.ticketmobileguard.row.ConteudoRowHolderInfo;

public class ConteudoAdapter extends BaseAdapter {
	
	private List<Conteudo> conteudos;
	private Activity context;
	
	public ConteudoAdapter(List<Conteudo> conteudos, Activity context){
		this.conteudos = conteudos;
		this.context = context;
	}

	@Override
	public int getCount() {
		return conteudos == null? 0: conteudos.size();
	}

	@Override
	public Conteudo getItem(int position) {
		return conteudos == null? null : conteudos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row;
		ConteudoRowHolderInfo holder;
		Conteudo conteudo = conteudos.get(position);
		if(convertView == null){
			holder = new ConteudoRowHolderInfo();
			LayoutInflater inflater = context.getLayoutInflater();
			row = inflater.inflate(R.layout.conteudo_row, null);
			holder.titulo = (TextView) row.findViewById(R.id.titulo);
			holder.description = (TextView) row.findViewById(R.id.description);
			
		}else{
			row =  convertView;
			holder = (ConteudoRowHolderInfo) row.getTag();
		}
		
		holder.drawRow(conteudo);
		row.setTag(holder);
		
		return row;
	}

}
